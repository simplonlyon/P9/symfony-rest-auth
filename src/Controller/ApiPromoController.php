<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Entity\Promo;
use App\Form\PromoType;

/**
 * @Route("/api/promo", name="api_promo")
 */
class ApiPromoController extends AbstractRestController
{
    public function __construct(SerializerInterface $serializer) {
        parent::__construct($serializer, Promo::class, PromoType::class);
    }    
}
