<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\AddressRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Address;
use App\Form\AddressType;

/**
 * @Route("/api/address", name="api_address")
 */
class ApiAddressController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function index()
    {
        $user = $this->getUser();
        $addresses = $user->getAddresses();

        // return JsonResponse::fromJsonString($this->serializer->serialize($addresses, 'json'));

        return new JsonResponse($this->serializer->serialize($addresses, 'json'), JsonResponse::HTTP_OK, [], true);
        
    }

    /**
     * @Route(methods="POST")
     */
    public function add(Request $request, ObjectManager $manager) {
        $address = new Address();
        $form = $this->createForm(AddressType::class, $address);
        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            $address->setUser($this->getUser());

            $manager->persist($address);
            $manager->flush();
            
            return new JsonResponse($this->serializer->serialize($address, 'json'), JsonResponse::HTTP_CREATED, [], true);

        }

        return $this->json($form->getErrors(true), JsonResponse::HTTP_BAD_REQUEST);
    }
}
