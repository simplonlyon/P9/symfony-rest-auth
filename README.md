# Projet Authentification JWT Symfony

## How To Use
1. cloner
2. `composer install`
3. `bin/console doctrine:database:create` puis `bin/console doctrine:migrations:migrate`
4. `./generate-keys` et donner 3 fois de suite la même passphrase (1234 par exemple)
5. On peut s'inscrire sur http://localhost:8000/api/register en donnant un objet dans ce format là
```
{
    "email":"email@valide.com",
    "password": {
        "first":"pass",
        "second":"pass
    },
    "age": 40
}
```
6. On peut s'authentifier sur http://localhost:8000/api/login_check en donnant ses credentials sous ce format là
```
{
    "username":"email@valide.com",
    "password":"pass"
}
```
7. Puis on accède aux pages protégées en rajoutant dans les headers de la request un header Authorization qui aura comme valeur `Bearer leToken`

## Etapes pour l'authentification JWT + User en base de données
1. On crée une classe User
2. On lui fait implémenter l'interface UserInterface (et donc ses 5 méthodes : getUsername, getPassword, getSalt, getRoles, eraseCredentials)
3. On va dans le security.yaml et on rajoute un provider pour notre entité User 
4. On rajoute également un encoder pour l'entité 
5. Lorsqu'on crée la méthode d'ajout de user, on oublie pas de faire l'encodage du mot de passe 
6. On installe le bundle pour l'authentification JWT (lexik/jwt-authentication-bundle)
7. On génère la clef public et la clef privée, soit en se basant sur la doc, soit en lançant le petit script qui se trouve dans generate-keys.sh
8. On rajoute dans le security.yaml un firewall global et un firewall pour le login 
9. On définit nos règles de sécurités dans les access control. Attention, à part la règle pour le /api/login, les règles ne sont pas les même selon les besoin de l'application 
10. On crée la route pour le login dans le routes.yaml